function [ ] = displayReconstructions( Vnorm, A, m, sampleSize )
    projector = Vnorm * Vnorm';
    original = reshape(A(:,1),[28,28]);
    reconstructed = reshape(projector * (double(A(:,1)) - m),[28,28]);
    for n = 2: sampleSize
        original = [original, reshape(A(:,n), [28,28])];
        reconstructed = [reconstructed, reshape(projector * (double(A(:,n))-m),[28,28]);];
    end
     figure('Name','Original image','NumberTitle','off'); imshow(original);
     figure('Name','Reconstructed image','NumberTitle','off'); imshow(reconstructed);
end

