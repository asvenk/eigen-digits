% Change no of training examples


trainingSize = [ 100 200 300 400 500 600 700 800 900 1000];
kvalues = [1]
accuracyEasy = zeros(1,10);
accuracyHard = zeros(1,10);
for j=1:size(kvalues, 2)
    k = kvalues(:,j);
    for i=1:size(trainingSize, 2)
        accuracyEasy(:,i) = setup(trainingSize(:,i), k, 5000);
    end
    for i=1:size(trainingSize, 2)
        accuracyHard(:,i) = setup(trainingSize(:,i), k, 0);
    end
    figureName = sprintf('K = %d',k);
    figure('Name',figureName,'NumberTitle','off'); 
    plot(trainingSize,accuracyEasy,trainingSize,accuracyHard)
    ylim([0 100])
    pause();
end

% pause();
% Change k  for traning size = 700 
trainingSize = [ 100 300 500 700 900 1000];
kvalues = [1 2 3 4 5 6 7 8 9 10]
for j=1:size(trainingSize, 2)
    d = trainingSize(:,j);
    for i=1:size(kvalues, 2)
        accuracyEasy(:,i) = setup(d, kvalues(:,i), 5000);
    end
    for i=1:size(kvalues, 2)
        accuracyHard(:,i) = setup(d, kvalues(:,i), 0);
    end
    figureName = sprintf('Training data size = %d',d);
    figure('Name',figureName,'NumberTitle','off'); 
    plot(kvalues,accuracyEasy,kvalues,accuracyHard)
    ylim([0 100])
    %pause();
end


