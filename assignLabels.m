function [ testLabels ] = assignLabels(N, datasetLabels)
    testLabels = zeros(size(N,1),1);
    for i=1:size(testLabels,1)
        labels = datasetLabels(:, N(i,:));
        [M, F] = mode(labels);
        % If the mode has a frequency of 1 then select the nearest
        % neighbour
        if F == 1
            testLabels(i,:) = datasetLabels(:, N(i,1));
        else
            testLabels(i,:) = M;
        end
    end
end

