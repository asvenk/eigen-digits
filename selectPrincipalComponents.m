function [ Vcut ] = selectPrincipalComponents( Vnorm, A, D, m, sampleSize )
    figure('Name','Plot of eigen values','NumberTitle','off');
    semilogx(D)
    size(D)
    diagsum = sum(D, 1)
    pcaDimension = 1;
    for pcaDimension = 1:size(D,1)
        pcaDiagSum = sum(D(1:pcaDimension), 1);
        ratio = pcaDiagSum / diagsum;
        if ratio >= 0.99
           break 
        end
    end
    Vcut = Vnorm(:,(1:pcaDimension));
    projector = Vcut * Vcut';
    reconstructed = reshape(projector * (double(A(:,1)) - m),[28,28]);
    for n = 2: sampleSize
        reconstructed = [reconstructed, reshape(projector * (double(A(:,n)) - m),[28,28]);];
    end
    %figure('Name','Lower dimension - PCA','NumberTitle','off'); imshow(reconstructed);
end

