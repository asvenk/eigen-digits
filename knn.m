function [ N ] = knn( A_norm, B_norm, k )

for i=1:size(B_norm,2)
    normDist = repmat(B_norm(:,i),1,size(A_norm,2));
    distances = A_norm - normDist;
    [~, I] = sort(sum(distances.^2));
    N(i,:) = I;
end
% Select only top k columns of the distances matrix
N = N(:,1:k);
end

