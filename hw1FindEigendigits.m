function [ m, V, D ] = hw1FindEigendigits( A )

[~, noOfTrainingEx] = size(A);
% Find mean of the images 
m = mean(A,2) ;
B = repmat(m,1,noOfTrainingEx);
A_norm = double(A) - B;
% Covariance matrix of A
cov = (A_norm'*A_norm)./ (noOfTrainingEx*1.0);
[V,D] = eig(cov);
V = A_norm * V ;
% Sort the eigen values
[D,I] = sort(diag(D), 'descend');
V = V(:, I);
end

