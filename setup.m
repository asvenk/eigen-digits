function [accuracy] = setup(datasize, k, offset)
% Setup the environment
% Load the data file

load('digits.mat');

testDatasize = 5000;
sampleSize = 20;
classifyDatasize = 1500;
% Convert Image to vector form. A is an array of vectorized images. 
dataset = squeeze(reshape(trainImages, [], 1, 1, size(trainImages, 4)));
testDataset = squeeze(reshape(testImages, [], 1, 1, size(testImages,4)));
datasetLabels = trainLabels;
testDatasetLabels = testLabels(:, (1+offset:testDatasize+offset));
% Select some no of images from the training data set. This should be less
% than the number of features, i.e 784.
A = dataset(:,(1:datasize));
B = testDataset(:,(1+offset:testDatasize+offset));

% Mean normalize the data and find the eigen vectors of covariance matrix
% of A. 
[m, V, D] = hw1FindEigendigits(A);

% Display some of the eigen vectors since these are of the same dimension.
displayEigenVectors(V, sampleSize);

% Normalize the eigen vectors to find a projecton into eigen space.
V_norm = normc(V);

% Project a sample of training set into the eigen space.
displayReconstructions(V_norm, A, m, sampleSize);
% pause();

% Examine the principal components and project onto reduced eigen space.
% Vnorm : Selecting n principal components
% C : Projection of into eigen space
[V_norm] = selectPrincipalComponents(V_norm, A, D, m, sampleSize);
classifierData = dataset(:,(1:datasize));
meanNorm = repmat(m,1,size(classifierData,2));
classifierData = (V_norm' * (double(classifierData) - meanNorm));
% pause();
% Display some resconstructions of test data.
displayReconstructions(V_norm, B, m, 20);
% pause();
% Normalize test data.
meanNorm = repmat(m,1,size(B,2));
B_norm = (V_norm' * (double(B) - meanNorm));
% Calculate the nearest neighbour.
N = knn( classifierData , B_norm, k );
% Assign labels to test data.
actualLabels = assignLabels(N, datasetLabels);
% Get the accuracy of the system.
accuracy = calclulateAccuracy(actualLabels, testDatasetLabels)*100
end

