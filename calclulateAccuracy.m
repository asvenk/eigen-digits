function [ accuracy ] = calclulateAccuracy(actualLabels, testDatasetLabels)
    actualLabels = uint8(actualLabels');
    error = 0;
    for i = 1:size(actualLabels,2)
        if actualLabels(:,i) ~= testDatasetLabels(:, i)
            error = error + 1;
        end
    end
    accuracy = 1 - (error / size(testDatasetLabels, 2));
end

