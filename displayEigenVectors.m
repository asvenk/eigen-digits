function [ ] = displayEigenVectors( V, sampleSize )
    A = reshape(V(:,1),[28,28]);
    for n = 2: sampleSize
        A = [A, reshape(V(:,n), [28,28])];
    end
    figure('Name','Eigen vector image','NumberTitle','off'); imshow(A);
end

